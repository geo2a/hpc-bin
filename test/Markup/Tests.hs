module Markup.Tests (markupTests) where

import System.Process
import Control.Monad (void)
import qualified System.FilePath as FP
import Test.Tasty (TestTree, testGroup)
import qualified Data.ByteString.Lazy.UTF8 as BS
import Test.Tasty.Golden (goldenVsString, goldenVsFile)
import Utils (runCommands)
import Data.List (intercalate)

inputBaseDir :: FilePath
inputBaseDir = FP.joinPath ["test", "Markup", "input"]

goldBaseDir :: FilePath
goldBaseDir = FP.joinPath ["test", "Markup", "gold"]

-- | Tests of the "hpc markup" subcommand
markupTests :: TestTree
markupTests = testGroup "markup" [helpTextTest, recipTest, recipTestIndex]

helpTextTest :: TestTree
helpTextTest = goldenVsString "Help" (goldBaseDir FP.</> "Help.stdout") go
  where
    go :: IO BS.ByteString
    go = runCommands "." ["hpc help markup"]

-- | Remove all generated *.html files in the Recip subdirectory
rmHtml :: IO ()
rmHtml = void $ readCreateProcess ((shell "rm -r *.html") { cwd = Just(inputBaseDir FP.</> "Recip") }) ""


rm :: [FilePath] -> IO ()
rm files = void $ readCreateProcess ((shell (intercalate " && " . map ("rm -r " <>) $ files)) { cwd = Just (inputBaseDir FP.</> "Recip")}) ""

recipTest :: TestTree
recipTest = goldenVsString "RecipStdout" (goldBaseDir FP.</> "Recip.stdout") go
  where
    go :: IO BS.ByteString
    go = runCommands (inputBaseDir FP.</> "Recip") ["hpc markup Recip.tix"] <* rmHtml

recipTestIndex :: TestTree
recipTestIndex = goldenVsFile "RecipIndexHtml" (goldBaseDir FP.</> "hpc_index.html") (inputBaseDir FP.</> "Recip" FP.</> "hpc_index.html") go
  where
    go :: IO ()
    go = do
        _ <- runCommands (inputBaseDir FP.</> "Recip") ["hpc markup Recip.tix"]
        rm ["hpc_index_alt.html", "hpc_index_fun.html", "hpc_index_exp.html", "Main.hs.html"]
        pure ()