{-# Language OverloadedStrings #-}
-- |
-- Module             : Trace.Hpc.Markup.Summary
-- Description        : Generating the summary html files.
module Trace.Hpc.Markup.Summary
  ( ModuleSummary (..),
    name_summary,
    fun_summary,
    exp_summary,
    alt_summary
  )
where

import qualified Lucid as L
import Data.List (sortBy)
import Data.Semigroup as Semi
import System.FilePath ((<.>))

index_name :: String
index_name = "hpc_index"

index_fun :: String
index_fun = "hpc_index_fun"

index_alt :: String
index_alt = "hpc_index_alt"

index_exp :: String
index_exp = "hpc_index_exp"

data ModuleSummary = ModuleSummary
  { expTicked :: !Int,
    expTotal :: !Int,
    topFunTicked :: !Int,
    topFunTotal :: !Int,
    altTicked :: !Int,
    altTotal :: !Int
  }
  deriving (Show)

showModuleSummary :: (String, String, ModuleSummary) -> String
showModuleSummary (modName, fileName, modSummary) =
  "<tr>\n"
    <> "<td>&nbsp;&nbsp;<tt>module <a href=\""
    <> fileName
    <> "\">"
    <> modName
    <> "</a></tt></td>\n"
    <> showSummary (topFunTicked modSummary) (topFunTotal modSummary)
    <> showSummary (altTicked modSummary) (altTotal modSummary)
    <> showSummary (expTicked modSummary) (expTotal modSummary)
    <> "</tr>\n"

showTotalSummary :: ModuleSummary -> String
showTotalSummary modSummary =
  "<tr style=\"background: #e0e0e0\">\n"
    <> "<th align=left>&nbsp;&nbsp;Program Coverage Total</tt></th>\n"
    <> showSummary (topFunTicked modSummary) (topFunTotal modSummary)
    <> showSummary (altTicked modSummary) (altTotal modSummary)
    <> showSummary (expTicked modSummary) (expTotal modSummary)
    <> "</tr>\n"

showSummary :: (Integral t, Show t) => t -> t -> String
showSummary ticked total =
  "<td align=\"right\">"
    <> showP (percent ticked total)
    <> "</td>"
    <> "<td>"
    <> show ticked
    <> "/"
    <> show total
    <> "</td>"
    <> "<td width=100>"
    <> ( case percent ticked total of
           Nothing -> "&nbsp;"
           Just w -> bar w ("bar" :: String)
       )
    <> "</td>"
  where
    showP Nothing = "-&nbsp;"
    showP (Just x) = show x <> "%"
    bar 0 _ = bar 100 "invbar"
    bar w inner =
      "<table cellpadding=0 cellspacing=0 width=\"100\" class=\"bar\">"
        <> "<tr><td><table cellpadding=0 cellspacing=0 width=\""
        <> show w
        <> "%\">"
        <> "<tr><td height=12 class="
        <> show inner
        <> "></td></tr>"
        <> "</table></td></tr></table>"

percent :: (Integral a) => a -> a -> Maybe a
percent ticked total = if total == 0 then Nothing else Just (ticked * 100 `div` total)

instance Semi.Semigroup ModuleSummary where
  (ModuleSummary eTik1 eTot1 tTik1 tTot1 aTik1 aTot1) <> (ModuleSummary eTik2 eTot2 tTik2 tTot2 aTik2 aTot2) =
    ModuleSummary (eTik1 + eTik2) (eTot1 + eTot2) (tTik1 + tTik2) (tTot1 + tTot2) (aTik1 + aTik2) (aTot1 + aTot2)

instance Monoid ModuleSummary where
  mempty =
    ModuleSummary
      { expTicked = 0,
        expTotal = 0,
        topFunTicked = 0,
        topFunTotal = 0,
        altTicked = 0,
        altTotal = 0
      }
  mappend = (<>)

summaryHtml :: [(String, String, ModuleSummary)] -> L.Html ()
summaryHtml mods =
  L.html_ ((L.toHtmlRaw header) <> (L.toHtmlRaw body))
  where
    header :: String
    header =
      "<head>"
        <> "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
        <> "<style type=\"text/css\">"
        <> "table.bar { background-color: #f25913; }\n"
        <> "td.bar { background-color: #60de51;  }\n"
        <> "td.invbar { background-color: #f25913;  }\n"
        <> "table.dashboard { border-collapse: collapse  ; border: solid 1px black }\n"
        <> ".dashboard td { border: solid 1px black }\n"
        <> ".dashboard th { border: solid 1px black }\n"
        <> "</style>\n"
        <> "</head>"

    body :: String
    body =
      "<body>"
        <> "<table class=\"dashboard\" width=\"100%\" border=1>\n"
        <> "<tr>"
        <> "<th rowspan=2><a href=\""
        <> index_name
        <> ".html\">module</a></th>"
        <> "<th colspan=3><a href=\""
        <> index_fun
        <> ".html\">Top Level Definitions</a></th>"
        <> "<th colspan=3><a href=\""
        <> index_alt
        <> ".html\">Alternatives</a></th>"
        <> "<th colspan=3><a href=\""
        <> index_exp
        <> ".html\">Expressions</a></th>"
        <> "</tr>"
        <> "<tr>"
        <> "<th>%</th>"
        <> "<th colspan=2>covered / total</th>"
        <> "<th>%</th>"
        <> "<th colspan=2>covered / total</th>"
        <> "<th>%</th>"
        <> "<th colspan=2>covered / total</th>"
        <> "</tr>"
        <> concat
          [ showModuleSummary (modName, fileName, modSummary)
            | (modName, fileName, modSummary) <- mods
          ]
        <> "<tr></tr>"
        <> showTotalSummary
          ( mconcat
              [ modSummary
                | (_, _, modSummary) <- mods
              ]
          )
        <> "</table></body>"

-- | Compute "hpc_index.html"
name_summary :: [(String, String, ModuleSummary)] -> (FilePath, L.Html ())
name_summary mods = (index_name <.> "html", summaryHtml (sortBy cmp mods))
  where
    cmp (n1, _, _) (n2, _, _) = compare n1 n2

-- | Compute "hpc_index_fun.html"
fun_summary :: [(String, String, ModuleSummary)] -> (FilePath, L.Html ())
fun_summary mods = (index_fun <.> "html", summaryHtml (sortBy cmp mods))
  where
    cmp (_, _, s1) (_, _, s2) = compare
      (percent (topFunTicked s2) (topFunTotal s2))
      (percent (topFunTicked s1) (topFunTotal s1))

-- | Compute "hpc_index_alt.html"
alt_summary :: [(String, String, ModuleSummary)] -> (FilePath, L.Html ())
alt_summary mods = (index_alt <.> "html", summaryHtml (sortBy cmp mods))
  where
    cmp (_, _, s1) (_, _, s2) = compare
      (percent (altTicked s2) (altTotal s2))
      (percent (altTicked s1) (altTotal s1))

-- | Compute "hpc_index_exp.html"
exp_summary :: [(String, String, ModuleSummary)] -> (FilePath, L.Html ())
exp_summary mods = (index_exp <.> "html", summaryHtml (sortBy cmp mods))
  where
    cmp (_, _, s1) (_, _, s2) = compare
      (percent (expTicked s2) (expTotal s2))
      (percent (expTicked s1) (expTotal s1))
